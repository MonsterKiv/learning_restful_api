<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Movie;
use App\Http\Resources\Movie as MovieResource;

class MovieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $movie = Movie::all();

        return response()->json([
            'status_code' => 200,
            'message' => 'All movies',
            'data' => MovieResource::collection($movie),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $movie = Movie::create($request->all());

        return response()->json([
            'status_code' => 200,
            'message' => 'Movie created',
            'data' => new MovieResource($movie),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $movie = Movie::findOrFail($id);

        return response()->json([
            'status_code' => 200,
            'message' => 'Found selected movie',
            'data' => new MovieResource($movie),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $movie = Movie::findOrFail($id);
        $movie->update($request->all());

        return response()->json([
            'status_code' => 200,
            'message' => 'Movie edited',
            'data' => new MovieResource($movie),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $movie = Movie::findOrFail($id);
        $movie->delete();

        return response()->json([
            'status_code' => 200,
            'message' => 'Movie deleted',
            'data' => new MovieResource($movie),
        ]);
    }
}
